var images = [
    "images/1.jpg",
    "images/2.jpg",
    "images/3.jpg",
    "images/koniec.jpg"
];

var imagesElem = [];
var imgsLoaded = 0;
var newImg;

for (var i = 0; i < images.length; i++) {
    newImg = new Image();
    newImg.src = images[i];

    newImg.onload = function () {
        imgsLoaded++;
        if (imgsLoaded === images.length) {
            animateCanvas();
        }
    };
    imagesElem.push(newImg);
}

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var currImg = 0;
var defSpeed = 16;
var anim;
var animateCanvas = function () {
    ctx.drawImage(imagesElem[currImg], 0, 0);
    currImg++;
    if (currImg === images.length - 1) {
        currImg = 0;
    }
    if (defSpeed < 3000) {
        anim = setTimeout(animateCanvas, defSpeed);
    }
};
var wait;
function slowDown() {
    defSpeed = 1.5 * defSpeed;
    clearTimeout(wait);

    if (defSpeed >= 3000) {
        gameover();
        setTimeout(animateCanvas, 5000);
        speedUp();
    } else {
        wait = setTimeout(speedUp, 3000);
    }
}

function speedUp() {
    defSpeed = defSpeed / 1.5;
    wait = setTimeout(speedUp, 3000);
}
c.addEventListener("click", slowDown);

function gameover() {
    clearTimeout(anim);
    ctx.drawImage(imagesElem[images.length - 1], 0, 0);
    // setTimeout(speedUp,2000);
}